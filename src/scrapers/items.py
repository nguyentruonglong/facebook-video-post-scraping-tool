from dataclasses import dataclass

@dataclass
class FacebookPost:
    """
    Represents a Facebook post item with various attributes.
    """

    # Declare data fields with default values.
    post_id: str = str()
    post_url: str = str()
    post_status: bool = True
    last_crawling: str = str()
    reaction_count: int = 0
    share_count: int = 0
    comment_count: int = 0
    video_thumbnail_url: str = str()
    photo_thumbnail_url: str = str()
    post_thumbnail_url: str = str()
    facebook_page_name: str = str()
    video_title: str = str()
    facebook_page_url: str = str()
    posted_at: str = str()
    embedded_video_url: str = str()

    def __init__(self,
                post_id: str = str(),
                post_url: str = str(),
                post_status: bool = True,
                last_crawling: str = str(),
                reaction_count: int = 0,
                share_count: int = 0,
                comment_count: int = 0,
                video_thumbnail_url: str = str(),
                photo_thumbnail_url: str = str(),
                post_thumbnail_url: str = str(),
                facebook_page_name: str = str(),
                video_title: str = str(),
                facebook_page_url: str = str(),
                posted_at: str = str(),
                embedded_video_url: str = str()):
        """
        Initialize a FacebookPost object with optional attributes.

        Args:
            post_id (str): Unique identifier for the post.
            post_url (str): URL of the Facebook post.
            post_status (bool): Status of the post.
            last_crawling (str): Timestamp of the last crawling.
            reaction_count (int): Number of reactions.
            share_count (int): Number of shares.
            comment_count (int): Number of comments.
            video_thumbnail_url (str): URL of the video thumbnail.
            photo_thumbnail_url (str): URL of the photo thumbnail.
            post_thumbnail_url (str): URL of the post thumbnail.
            facebook_page_name (str): Name of the Facebook page.
            video_title (str): Title of the video (if applicable).
            facebook_page_url (str): URL of the Facebook page.
            posted_at (str): Timestamp of when the post was made.
            embedded_video_url (str): URL of embedded video (if applicable).
        """
        # Initialize the object's attributes with provided or default values.
        self.post_id = post_id
        self.post_url = post_url
        self.post_status = post_status
        self.last_crawling = last_crawling
        self.reaction_count = reaction_count
        self.share_count = share_count
        self.comment_count = comment_count
        self.video_thumbnail_url = video_thumbnail_url
        self.photo_thumbnail_url = photo_thumbnail_url
        self.post_thumbnail_url = post_thumbnail_url
        self.facebook_page_name = facebook_page_name
        self.video_title = video_title
        self.facebook_page_url = facebook_page_url
        self.posted_at = posted_at
        self.embedded_video_url = embedded_video_url
