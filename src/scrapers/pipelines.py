import json
import sys
import requests
from threading import Thread
from settings import HOSTNAME, API_FACEBOOK_POST_PATH, TIMEOUT
from utils.io import DataIO

class WebservicePipeline:
    """
    Pipeline for sending data to a web service.
    """

    def __init__(self, scraper_arguments):
        """
        Initialize the WebservicePipeline.

        Args:
            scraper_arguments (dict): Scraper arguments.
        """
        self.scraper_arguments = scraper_arguments

    def send_post_request(self, url, data):
        """
        Send a POST request to a specified URL with the provided data.

        Args:
            url (str): The URL to send the POST request to.
            data (dict): The data to include in the POST request.

        Raises:
            RequestException: If an HTTP request exception occurs.
        """
        try:
            # Define headers for the HTTP request.
            request_headers = {'content-type': 'application/json'}

            # Send a POST request with JSON data, ignoring SSL verification, and setting a timeout.
            response = requests.post(
                url, data=json.dumps(data), headers=request_headers, verify=False, timeout=TIMEOUT)

            # Log the HTTP response status code.
            error_message = f'HTTP response status code for request to {data.get("post_url", "")}: {response.status_code}'
            DataIO().write_to_log(error_message)

        except Exception as e:
            # Handle HTTP request exceptions.
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {sys.exc_info()[-1].tb_lineno}: {str(e)}'
            DataIO().write_to_log(error_message)

    def push_to_webservice(self, data):
        """
        Push data to a web service asynchronously using a separate thread.

        Args:
            data (dict): The data to send to the web service.
        """
        try:
            # Create the full URL for the web service endpoint.
            full_api_url = HOSTNAME + API_FACEBOOK_POST_PATH

            # Start a separate thread to send the POST request.
            Thread(target=self.send_post_request, args=[full_api_url, data]).start()

        except Exception as e:
            # Handle general exceptions.
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {sys.exc_info()[-1].tb_lineno}: {str(e)}'
            DataIO().write_to_log(error_message)
