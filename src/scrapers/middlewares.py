import re
import sys
import time
import urllib
from urllib.parse import urljoin

import dateparser
import dateutil
import pytz
import settings
import validators
from bs4 import BeautifulSoup
from furl.furl import furl
from utils.io import DataIO
from .downloader import Downloader
from .items import FacebookPost

class DownloaderMiddleware:
    def __init__(self, scraper_arguments):
        """
        Initialize the DownloaderMiddleware with scraper_arguments arguments.

        Args:
            scraper_arguments (dict): Dictionary containing scraper-specific arguments.
        """
        # Store the provided scraper arguments in the middleware instance.
        self.scraper_arguments = scraper_arguments

    def process_html_response(self, chrome_driver):
        """
        Process and retrieve the HTML response from the Chrome driver.

        Args:
            chrome_driver (WebDriver): The Chrome driver instance.

        Returns:
            str: The page source as a string.
        """
        # Initialize an empty string to store the page source.
        page_source = str()
        try:
            # Retrieve the page source from the Chrome driver.
            page_source = chrome_driver.page_source
        except Exception as e:
            # Handle any exceptions that may occur.
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the page source, even if an exception occurred.
            return page_source

    def process_facebook_cookie(self, chrome_driver):
        """
        Process and set Facebook cookies in the Chrome driver.

        Args:
            chrome_driver (WebDriver): The Chrome driver instance.

        Returns:
            chrome_driver (WebDriver): The Chrome driver instance with Facebook cookies.
        """
        try:
            # Visit the URL where Facebook cookies can be set.
            chrome_driver.get(settings.COOKIE_URL)
            # Load Facebook cookies from a file.
            facebook_cookies = DataIO().load_cookies(settings.COOKIE_FILE_DIR)
            # Iterate through the loaded cookies and add them to the Chrome driver.
            for cookie in facebook_cookies:
                # Remove the 'sameSite' attribute from each cookie.
                cookie.pop('sameSite')
                # Add the cookie to the Chrome driver.
                chrome_driver.add_cookie(cookie)
        except Exception as e:
            # Handle any exceptions that may occur.
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the Chrome driver instance with added Facebook cookies.
            return chrome_driver

class ScraperMiddleware(Downloader, DownloaderMiddleware):
    def __init__(self, scraper_arguments):
        """
        Initialize the ScraperMiddleware with scraper_arguments arguments.

        Args:
            scraper_arguments (dict): Dictionary containing scraper-specific arguments.
        """
        # Call the constructors of the parent classes Downloader and DownloaderMiddleware.
        # These parent classes are used to provide functionality to the middleware.
        super().__init__(scraper_arguments)

    def process_post_status(self, page_source):
        """
        Check if a Facebook post is accessible or not.

        Args:
            page_source (str): The page source as a string.

        Returns:
            bool: True if the post is accessible, False otherwise.
        """
        try:
            # Initialize a flag to False to indicate an inaccessible post.
            is_accessible = False
            # Check if specific error messages are not present in the page source.
            if all(warning.lower() not in page_source.lower() for warning in [
                "You must log in to continue.",
                "This Page Isn't Available",
                "The link may be broken",
                "The link you followed may be broken",
                "This Content Isn't Available Right Now",
                "Bạn phải đăng nhập để tiếp tục."
            ]):
                is_accessible = True
        except Exception as e:
            # Handle exceptions and set the flag to False.
            is_accessible = False
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the is_accessible, indicating whether the post is accessible.
            return is_accessible

    def remove_invalid_characters(self, input_string):
        """
        Remove invalid characters and spaces from a string.

        Args:
            input_string (str): The input string.

        Returns:
            str: The cleaned string.
        """
        try:
            # Replace invalid characters and multiple spaces with a single space.
            cleaned_string = re.sub(
                r'[^0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹäöüßÄÖÜẞ]+',
                ' ', input_string)
            cleaned_string = re.sub(r'[\s]+', ' ', cleaned_string)
            cleaned_string = str(cleaned_string).strip()
        except Exception as e:
            # Handle exceptions and return an empty string.
            cleaned_string = str()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the cleaned cleaned_string.
            return cleaned_string

    def convert_string_to_number(self, number_string):
        """
        Convert a string representation of a number with K/M/B suffixes to an integer.

        Args:
            number_string (str): The number as a string.

        Returns:
            int: The converted number.
        """
        try:
            # Initialize the converted_number as a float.
            converted_number = 0.0
            # Define a dictionary mapping suffixes to multiplier values.
            char_value_dict = {'K': 1000, 'M': 1000000, 'B': 1000000000}
            # Check if the input string contains K, M, or B suffixes.
            if all(key not in number_string for key in ['K', 'M', 'B']):
                # If not, convert the string to float.
                converted_number = float(number_string)
            else:
                # If yes, calculate the number by multiplying with the corresponding multiplier.
                converted_number = float(
                    number_string[:-1]) * char_value_dict[number_string[-1]]
                # Convert the result to an integer.
                converted_number = int(converted_number)
        except Exception as e:
            # Handle exceptions and return 0.
            converted_number = 0
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the converted converted_number.
            return converted_number

    def convert_string_into_datetime(self, input_string):
        """
        Convert a string into a formatted datetime string.

        Args:
            input_string (str): The input string.

        Returns:
            str: The formatted datetime string.
        """
        try:
            # Initialize the formatted_datetime_string as an empty string.
            formatted_datetime_string = str()
            # Parse the input string into a datetime object and format it.
            formatted_datetime_string = dateparser.parse(
                input_string).strftime('%Y-%m-%d %H:%M:%S')
            # Convert the formatted datetime to the specified timezone.
            formatted_datetime_string = dateutil.parser.parse(formatted_datetime_string).astimezone(
                pytz.timezone(settings.TIMEZONE)).strftime('%Y-%m-%d %H:%M:%S')
        except Exception as e:
            # Handle exceptions and return an empty string.
            formatted_datetime_string = str()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the formatted formatted_datetime_string.
            return formatted_datetime_string

    def extract_facebook_video_url(self, page_source):
        """
        Extract the Facebook video URL from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            str: The extracted video URL.
        """
        extracted_video_url = str()

        try:
            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')

            # List of selectors to try for extracting video URL
            selectors = ['._2c9v._53mv[src]', '._53mw']

            for selector in selectors:
                video_element = soup.select_one(selector)

                if video_element:
                    video_element_source = video_element.prettify()

                    # Use regular expressions to extract the video URL
                    matched_video_urls = re.search('"type":"video","src":"([^",]+?)",', video_element_source)
                    if matched_video_urls:
                        extracted_video_url = matched_video_urls[1].replace('"type":"video","src":"', str()).replace('",', str())
                        extracted_video_url = extracted_video_url.replace('\\', str()).replace('&amp;', '&')
                        break  # Video URL found, exit the loop

            # If video URL is still empty, attempt other methods
            if not extracted_video_url:
                other_selectors = ['"src":"(.+?)"', 'hd_src:"(.+?)"', 'sd_src:"(.+?)"']
                for other_selector in other_selectors:
                    matched_video_urls = re.search(other_selector, page_source)
                    if matched_video_urls:
                        extracted_video_url = matched_video_urls[1].replace(other_selector.split(':')[0], str())
                        break  # Video URL found, exit the loop

        except Exception as e:
            # Handle exceptions and return an empty string.
            extracted_video_url = str()
            current_class = self.__class__.__name__

            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

        return extracted_video_url

    def extract_facebook_post_id(self, page_source):
        """
        Extract the Facebook post ID from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            str: The extracted post ID.
        """
        try:
            # Initialize post_id as an empty string.
            post_id = str()
            
            # List of regex patterns to search for post ID substrings.
            patterns = [r'post_id\.[0-9]{10,25}', r'VK:[0-9]{10,25}']
            
            for pattern in patterns:
                matched_id_substrs = re.findall(pattern, page_source)
                if len(matched_id_substrs) > 0:
                    # If found, extract the post ID.
                    post_id = matched_id_substrs[0].split('.')[-1] if pattern.startswith('post_id') else matched_id_substrs[0].replace('VK:', str())
                    break  # Exit the loop if post ID is found.

        except Exception as e:
            # Handle exceptions and return an empty string.
            post_id = str()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted post_id.
            return post_id

    def extract_reaction_count(self, page_source):
        """
        Extract the reaction count from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            int: The extracted reaction count.
        """
        try:
            # Initialize reaction_count_number as 0.
            reaction_count_number = 0
            reaction_count_string = str()

            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')

            # List of CSS classes to search for reaction count.
            css_classes = ['_1g06', 'span.tojvnm2t.a6sixzi8.abs2jz4q', '_81hb']

            for css_class in css_classes:
                # Attempt to find the reaction count element with specific classes.
                reaction_count_element = soup.select_one(f'.{css_class}')
                if reaction_count_element:
                    reaction_count_string = reaction_count_element.get_text()
                    break  # Exit the loop if reaction count is found.

            # Clean and format the reaction count string.
            reaction_count_string = reaction_count_string.strip().replace(',', '.')
            # Use regular expressions to extract the count portion of the string.
            matched_reaction_group = re.search(
                r'^(\d+(\.[\d]+)?[K|M|B]?)', reaction_count_string)
            matched_reaction_count_number = matched_reaction_group.group(
                1) if matched_reaction_group else '0.0'

            # Convert the string to an integer using the helper function.
            reaction_count_number = self.convert_string_to_number(
                matched_reaction_count_number)
        except Exception as e:
            # Handle exceptions and return 0.
            reaction_count_number = 0
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted reaction_count_number.
            return reaction_count_number

    def extract_share_count(self, page_source):
        """
        Extract the share count from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            int: The extracted share count.
        """
        try:
            # Initialize share_count_number as 0.
            share_count_number = 0
            share_count_string = str()

            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')

            # List of CSS selectors to search for share count.
            css_selectors = ['div._1fnt span._1j-c:nth-child(2)', '._3rwx._42ft']

            for css_selector in css_selectors:
                # Attempt to find the share count element with specific CSS selectors.
                share_count_element = soup.select_one(css_selector)
                if share_count_element:
                    share_count_string = share_count_element.get_text()
                    break  # Exit the loop if share count is found.

            # Clean and format the share count string.
            share_count_string = share_count_string.strip().replace(',', '.')
            # Use regular expressions to extract the count portion of the string.
            matched_share_group = re.search(
                r'^(\d+(\.[\d]+)?[K|M|B]?)', share_count_string)
            matched_share_count_number = matched_share_group.group(
                1) if matched_share_group else '0.0'

            # Convert the string to an integer using the helper function.
            share_count_number = self.convert_string_to_number(
                matched_share_count_number)
        except Exception as e:
            # Handle exceptions and return 0.
            share_count_number = 0
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted share_count_number.
            return share_count_number

    def extract_comment_count(self, page_source):
        """
        Extract the comment count from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            int: The extracted comment count.
        """
        try:
            # Initialize comment_count_number as 0 and comment_count_string as an empty string.
            comment_count_number = 0
            comment_count_string = str()

            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')

            # List of CSS selectors to search for comment count.
            css_selectors = ['div._1fnt span._1j-c[data-sigil]', 'span.d2edcug0.hpfvmrgz.qv66sw1b', '._3hg-._42ft']

            for css_selector in css_selectors:
                # Attempt to find the comment count element with specific CSS selectors.
                comment_count_element = soup.select_one(css_selector)
                if comment_count_element:
                    # If found, extract the text.
                    comment_count_string = comment_count_element.get_text()
                    break  # Exit the loop if comment count is found.

            # Clean and normalize the comment_count_string.
            comment_count_string = comment_count_string.strip().replace(',', '.')

            # Use regular expressions to extract the numeric part of the string.
            matched_comment_group = re.search(
                r'^(\d+(\.[\d]+)?[K|M|B]?)', comment_count_string)
            matched_comment_count_number = matched_comment_group.group(
                1) if matched_comment_group else '0.0'

            # Convert the matched numeric string to a number.
            comment_count_number = self.convert_string_to_number(
                matched_comment_count_number)
        except Exception as e:
            # Handle exceptions and set comment_count_number to 0.
            comment_count_number = 0
            current_class = self.__class__.__name__

            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted comment_count_number.
            return comment_count_number

    def extract_video_thumbnail_url(self, page_source):
        """
        Extract the video thumbnail URL from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            str: The extracted video thumbnail URL.
        """
        try:
            # Initialize video_thumbnail_url as an empty string.
            video_thumbnail_url = str()

            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')
            # Attempt to find the embedded video element with specific classes.
            embedded_video_element = soup.select_one('.img._lt3._4s0y')
            if embedded_video_element:
                # If found, prettify the element source and clean it.
                video_element_source = embedded_video_element.prettify()
                video_element_source = video_element_source.replace(
                    '\\3a ', ':').replace('\\3d ', '=').replace('\\26 ', '&')
                # Use regular expressions to extract the video thumbnail URL.
                matched_substrs = re.findall(
                    'style="background: url\(\'([^\)\']+)\'\)', video_element_source)
                if matched_substrs:
                    video_thumbnail_url = matched_substrs[0].replace(
                        'style="background: url(\'', str()).replace('\')', str())
                    # URL-decode the extracted URL.
                    video_thumbnail_url = urllib.parse.unquote_plus(
                        video_thumbnail_url, encoding='utf-8', errors='replace')
            else:
                # Try to find an alternative thumbnail URL.
                alternative_thumbnail_elements = soup.select('img._4lpf[src]')
                for element in alternative_thumbnail_elements:
                    video_thumbnail_url = element['src']
                    break  # Use the first alternative thumbnail URL found.

        except Exception as e:
            # Handle exceptions and return an empty string.
            video_thumbnail_url = str()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted video_thumbnail_url.
            return video_thumbnail_url

    def extract_photo_thumbnail_url(self, page_source):
        """
        Extract the photo thumbnail URL from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            str: The extracted photo thumbnail URL.
        """
        try:
            # Initialize photo_thumbnail_url as an empty string.
            photo_thumbnail_url = str()

            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')
            # Attempt to find the thumbnail element with specific attributes.
            thumbnail_elements = soup.select('._1ktf a[data-ploi]')
            for thumbnail_element in thumbnail_elements:
                # If found, extract the "data-ploi" attribute.
                photo_thumbnail_url = thumbnail_element["data-ploi"]
                break  # Use the first thumbnail URL found.

            if not photo_thumbnail_url:
                # Try to find an alternative thumbnail URL.
                alternative_thumbnail_elements = soup.select('._6ks img[src]')
                for thumbnail_element in alternative_thumbnail_elements:
                    photo_thumbnail_url = thumbnail_element['src']
                    break  # Use the first alternative thumbnail URL found.

        except Exception as e:
            # Handle exceptions and return an empty string.
            photo_thumbnail_url = str()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted photo_thumbnail_url.
            return photo_thumbnail_url

    def extract_page_name(self, page_source):
        """
        Extract the page name from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            str: The extracted page name.
        """
        try:
            # Initialize page_name as an empty string.
            page_name = str()

            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')
            
            # List of selectors to attempt to find the page name.
            selectors = ['._52jd._52jb._52jh._5qc3._4vc-._3rc4._4vc- a',
                        'span.fwb.fcg']
            
            # Attempt to find the page name element using different selectors.
            for selector in selectors:
                page_name_element = soup.select_one(selector)
                if page_name_element is not None:
                    # If found, extract the text.
                    page_name = page_name_element.get_text()
                    break  # Stop when the page name is found.

            # Remove invalid characters and spaces from the extracted name.
            page_name = self.remove_invalid_characters(page_name)
            
        except Exception as e:
            # Handle exceptions and return an empty string.
            page_name = str()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted page_name.
            return page_name

    def extract_video_title(self, page_source):
        """
        Extract the video title from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            str: The extracted video title.
        """
        try:
            # Initialize video_title as an empty string.
            video_title = str()

            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')

            # List of selectors to attempt to find the video title.
            selectors = ['h3._52jh._4act._16-5 > span', 'span._2iek._50f7']

            # Attempt to find the video title element using different selectors.
            for selector in selectors:
                video_title_element = soup.select_one(selector)
                if video_title_element is not None:
                    # If found, extract the text.
                    video_title = video_title_element.get_text()
                    break  # Stop when the video title is found.

            # Clean the extracted title by removing invalid characters.
            video_title = self.remove_invalid_characters(video_title)
            
        except Exception as e:
            # Handle exceptions by setting video_title to an empty string and logging the error.
            video_title = str()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted and cleaned video title.
            return video_title

    def extract_facebook_page_url(self, page_source):
        """
        Extract the Facebook page URL from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            str: The extracted Facebook page URL.
        """
        try:
            facebook_page_url = str()
            video_post_string = str()

            soup = BeautifulSoup(page_source, 'html.parser')

            video_post_selectors = ['._52jd._52jb._52jh._5qc3._4vc-._3rc4._4vc- a[href]', 'span.fsm.fwn.fcg > a[href]']

            # Attempt to find the video post URL element using different selectors.
            for selector in video_post_selectors:
                video_post_element = soup.select_one(selector)
                if video_post_element:
                    # If found, extract the URL string.
                    video_post_string = video_post_element['href']
                    break  # Stop when the URL is found.

            # Ensure that the video post URL is valid by joining it with the base URL.
            video_post_string = urljoin(settings.BASE_URL, video_post_string)

            # Check if the joined URL is a valid URL and not permalink.php.
            if validators.url(video_post_string) and 'permalink.php' not in video_post_string:
                # Parse the URL structure using furl library.
                url_structure = furl(video_post_string.strip()).asdict()

                # Extract the Facebook page URL by combining the origin and the first path segment.
                facebook_page_url = url_structure.get('origin', str()) + '/' + url_structure.get('path', {}).get('segments', [''])[0]

        except Exception as e:
            # Handle exceptions by setting facebook_page_url to an empty string and logging the error.
            facebook_page_url = str()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return facebook_page_url

    def extract_posted_at(self, page_source):
        """
        Extract the posted date and time from the page source.

        Args:
            page_source (str): The page source as a string.

        Returns:
            str: The extracted posted date and time as a string.
        """
        try:
            # Initialize variables for posted date and time.
            posted_datetime = str()

            # Parse the page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')

            # List of selectors to attempt to find the posted date and time element.
            selectors = ['._52jc._5qc4._78cz._24u0._36xo a', 'a._5pcq > abbr._5ptz[title]']

            # Attempt to find the posted date and time element using different selectors.
            for selector in selectors:
                posted_at_element = soup.select_one(selector)
                if posted_at_element is not None:
                    # If found, extract the text or title attribute.
                    if 'title' in posted_at_element.attrs:
                        posted_datetime = posted_at_element['title']
                    else:
                        posted_datetime = posted_at_element.get_text()
                    break  # Stop when the date and time are found.

            # Convert the extracted date and time string into a datetime object.
            posted_datetime = self.convert_string_into_datetime(posted_datetime)

        except Exception as e:
            # Handle exceptions by setting posted_datetime to None and logging the error.
            posted_datetime = None
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

        finally:
            # Return the extracted and converted posted date and time.
            return posted_datetime

    def parse_facebook_post_info(self, page_source, facebook_post_url):
        """
        Parse information about a Facebook post from the page source.

        Args:
            page_source (str): The page source as a string.
            facebook_post_url (str): The URL of the Facebook post.

        Returns:
            dict: A dictionary containing information about the Facebook post.
        """
        try:
            post_info = dict()

            # Process the post status from the page source.
            status = self.process_post_status(page_source)

            # Get the current time as a formatted string.
            current_time = DataIO().get_current_time().strftime('%Y-%m-%d %H:%M:%S.%f')

            if status:
                # If the post status is valid, create a FacebookPost object with details.
                facebook_post_url = facebook_post_url.replace('https://m.', 'https://')
                post_info = FacebookPost(
                    post_id=self.extract_facebook_post_id(page_source),
                    post_url=facebook_post_url,
                    post_status=status,
                    last_crawling=current_time,
                    reaction_count=self.extract_reaction_count(page_source),
                    share_count=self.extract_share_count(page_source),
                    comment_count=self.extract_comment_count(page_source),
                    video_thumbnail_url=self.extract_video_thumbnail_url(page_source),
                    photo_thumbnail_url=str(),
                    post_thumbnail_url=str(),
                    facebook_page_name=self.extract_page_name(page_source),
                    video_title=self.extract_video_title(page_source),
                    facebook_page_url=self.extract_page_url(page_source),
                    posted_at=self.extract_posted_at(page_source),
                    embedded_video_url=self.extract_facebook_video_url(page_source),
                ).__dict__
            else:
                # If the post status is invalid, create a minimal FacebookPost object.
                post_info = FacebookPost(
                    post_url=facebook_post_url,
                    post_status=status,
                    last_crawling=current_time
                ).__dict__

        except Exception as e:
            # Handle exceptions by setting post_info to an empty dictionary and logging the error.
            post_info = dict()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the parsed Facebook post information.
            return post_info

    def extract_facebook_post_page_source(self, chrome_driver, facebook_post_url):
        """
        Extract the page source of a Facebook post using a web driver.

        Args:
            chrome_driver (WebDriver): An initialized Chrome web driver.
            facebook_post_url (str): The URL of the Facebook post.

        Returns:
            str: The extracted page source as a string.
        """
        try:
            extracted_page_source = str()

            # Load the Facebook post URL in the web driver.
            chrome_driver.get(facebook_post_url)
            time.sleep(10)
            current_url = chrome_driver.current_url

            if 'https://m.' not in current_url:
                # If the current URL is not in the mobile format, switch to the mobile version.
                current_url = facebook_post_url.replace('www.', str()).replace('https://', 'https://m.')
                chrome_driver.get(current_url)
                time.sleep(10)

            # Get the page source after loading the URL.
            extracted_page_source = chrome_driver.page_source
        except Exception as e:
            # Handle exceptions by setting extracted_page_source to an empty string and logging the error.
            extracted_page_source = str()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted page source.
            return extracted_page_source

    def process_facebook_post(self, facebook_post_url):
        """
        Process the output data for a Facebook post URL.

        Args:
            facebook_post_url (str): The URL of the Facebook post.

        Returns:
            dict: A dictionary containing processed information about the Facebook post.
        """
        try:
            page_source = str()
            processed_data = dict()

            # Initialize a Chrome web driver.
            chrome_driver = self.initialize_chrome_driver()

            # Process Facebook cookies using the web driver.
            chrome_driver = self.process_facebook_cookie(chrome_driver)

            # Extract the page source of the Facebook post.
            page_source = self.extract_facebook_post_page_source(chrome_driver, facebook_post_url)

            # Parse information about the Facebook post from the page source.
            processed_data = self.parse_facebook_post_info(page_source, facebook_post_url)

            # Close the Chrome web driver.
            self.close_chrome_driver(chrome_driver)
        except Exception as e:
            # Handle exceptions by setting processed_data to an empty dictionary and logging the error.
            processed_data = dict()
            current_class = self.__class__.__name__
            # Log the error message and line number.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the processed output data.
            return processed_data
