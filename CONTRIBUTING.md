# Contributing to the Facebook Video Post Scraping Tool

Thank you for considering contributing to the Facebook Video Post Scraping Tool. By contributing, you help make this tool better for everyone. Before you get started, please take a moment to review the following guidelines.

## Code of Conduct

Please note that we have a Code of Conduct in place to ensure a welcoming and inclusive community for all contributors and users. We expect all contributors to adhere to these guidelines. You can review our Code of Conduct [here](CODE_OF_CONDUCT.md).

## Reporting Issues

If you encounter any issues or have suggestions for improvements, please check the [GitHub Issues](https://gitlab.com/nguyentruonglong/facebook-video-post-scraping-tool/-/issues) page to see if the issue has already been reported. If not, you can open a new issue, providing as much detail as possible, including the steps to reproduce the issue and your environment.

## Feature Requests

We welcome feature requests that align with the goals and purpose of the Facebook Video Post Scraping Tool. Please create a new issue on GitHub to propose your idea. Clearly explain the feature, its use case, and why it would be valuable.

## Pull Requests

We appreciate contributions from the community. If you'd like to contribute code or documentation, please follow these steps:

1. Fork the repository to your GitHub account.
2. Clone your forked repository to your local machine.

    ```
    git clone https://gitlab.com/nguyentruonglong/facebook-video-post-scraping-tool.git
    ```

3. Create a new branch for your contribution.

    ```
    git checkout -b feature/your-feature-name
    ```

4. Make your changes, ensuring that your code adheres to the project's coding standards.

5. Test your changes thoroughly to avoid introducing new issues.

6. Commit your changes and push them to your forked repository.

    ```
    git commit -m "Add your commit message here"
    git push origin feature/your-feature-name
    ```

7. Open a pull request on the main repository. Please provide a clear and concise description of your changes in the pull request.

8. Your pull request will be reviewed, and feedback may be provided.

9. Once your pull request is approved, it will be merged into the main repository.

## Licensing

By contributing to the Facebook Video Post Scraping Tool, you agree that your contributions will be subject to the terms and conditions of the project's [LICENSE](LICENSE). Your contributions must be your original work, and you must have the right to license them under the project's license.

Thank you for contributing to the Facebook Video Post Scraping Tool!
